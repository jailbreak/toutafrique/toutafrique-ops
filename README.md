# Tout Afrique Operations

This repository contains [Kubernetes](https://kubernetes.io/) resource manifests that are deployed via [ArgoCD](https://argoproj.github.io/argo-cd/) to the cluster, implementing [GitOps](gitops.tech) practices.

## Configuration

[Install kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) and download your kubeconfig YAML file and export the `KUBECONFIG` env var from the cluster provider ([Scaleway](https://console.scaleway.com/)).

## Cluster setup

```bash
cd cluster

kubectl edit daemonset traefik -n kube-system
# add this arg to traefik command: --providers.kubernetesIngress.ingressClass=traefik-cert-manager

kubectl apply -f clusterissuer.yaml
```

### GitOps with Argo CD

Install Argo CD in cluster, following https://argoproj.github.io/argo-cd/getting_started/

```bash
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
```

## Web API

```bash
kubectl create namespace afd-toutafrique

# Replace xxx with real values, get them from the cluster provider.
kubectl create secret generic --namespace afd-toutafrique s3-bucket-credentials --from-literal=AWS_ACCESS_KEY_ID=xxx --from-literal=AWS_SECRET_ACCESS_KEY=xxx
```

The other resources are synced automatically via the GitOps operator (ArgoCD), so just change and commit those files in this repository.

## Frontend

For now it's done manually: cf [toutafrique-frontend](https://framagit.org/jailbreak/toutafrique/toutafrique-frontend#production)
